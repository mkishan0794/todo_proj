# TO DO Backend Api's

### Setting Up Environment

RUN `virtualenv -p python3 <env_name>`

RUN `source bin/activate`

### Cloning Repository

`git clone https://mkishan0794@bitbucket.org/mkishan0794/todo_proj.git`


## Installing Dependencies

#### Installing Python Packages

RUN `pip3 install -r requirements.txt` inside the project base directory

### Other Dependencies needed to be Installed

1) Redis
2) Postgres
3) MongoDB

##### Make Sure All the above dependencies are up and running


## Running The App Locally

1) Running Python App

        a) RUN `python3 manage.py runserver 0.0.0.0:3000` in the base directory

2) Running Celery Workers
	
	    a) RUN `celery -A todo_proj worker -l info` in New Terminal
	
    	b) RUN `celery -A todo_proj beat -l info` in New Terminal


## Postman Link Fot API Testing

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/8a9a54b55961e4d95ffd)
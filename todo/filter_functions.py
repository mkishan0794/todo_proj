#Python Imports
import datetime
from datetime import timedelta

def this_week_range(date):
    year, week, dow = date.isocalendar()

    if dow == 7:
        start_date = datetime.date.today()
    else:
        start_date = datetime.date.today() - timedelta(dow)

    end_date = start_date + timedelta(6)

    return (start_date, end_date)
    
def next_week_range(date):
    year, week, dow = date.isocalendar()

    if dow == 7:
        start_date = datetime.date.today() + timedelta(days=7)
    else:
        start_date = datetime.date.today() + timedelta(days=7) - timedelta(dow)
        
    end_date = start_date + timedelta(6)

    return (start_date, end_date)

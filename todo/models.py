from django.db import models

# Create your models here.
from mongoengine import fields, Document, EmbeddedDocument


class SubTasks(EmbeddedDocument):
    title           = fields.StringField()
    description     = fields.StringField()
    is_completed    = fields.BooleanField(default=False)

class Task(Document):
    title           = fields.StringField()
    description     = fields.StringField()
    due_date        = fields.DateTimeField()
    is_completed    = fields.BooleanField(default=False) 
    sub_tasks       = fields.ListField(fields.EmbeddedDocumentField(SubTasks))
    is_deleted      = fields.BooleanField(default=False)
    deleted_on      = fields.DateTimeField()

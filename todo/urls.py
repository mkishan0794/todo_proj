from django.urls import path

from . views import(
    CreateToDoAPI,
    TasksListAPI,
    SearchTaskAPI,
    TaskDetailAPI,
    TaskUpdateAPI,
    )


urlpatterns = [
    path('create/', CreateToDoAPI.as_view(), name='todo-create'),
    path('list/', TasksListAPI.as_view(), name='todo-list'),
    path('search/', SearchTaskAPI.as_view(), name='search-task'),
    path('detail/', TaskDetailAPI.as_view(), name='detail-task'),
    path('update-task/', TaskUpdateAPI.as_view(), name='update-task'),
    ]
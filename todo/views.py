#Django Imports
from django.shortcuts import render

#DRF Imports
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

#Mongoengine Imports
from mongoengine import DoesNotExist
from mongoengine.queryset.visitor import Q

#Custom Imports
from . serializers import *
from . models import Task, SubTasks
from . filter_functions import *

#Python Imports
import datetime
from datetime import timedelta



class CreateToDoAPI(APIView):
    def post(self, request):
        serializer = AddToDoSerializer(data=request.data)
        if serializer.is_valid():
            sub_tasks_list = []
            for item in range(0, len(request.data["subTasks"])):
                sub_task_item = SubTasks(
                    title           = request.data["subTasks"][item]["title"],
                    description     = request.data["subTasks"][item]["description"],
                    is_completed    = False
                )
                sub_tasks_list.append(sub_task_item)
            task_obj = Task(
                title           = request.data["title"],
                description     = request.data["description"],
                due_date        = request.data["dueDate"],
                is_completed    = False,
                sub_tasks       = sub_tasks_list
            )
            task_obj.save()
            return Response({"message":"Task added successfully","requestStatus":1},status=status.HTTP_200_OK)
        return Response({"data":serializer.errors,"message":"enter all required fields","requestStatus":0},status=status.HTTP_400_BAD_REQUEST)


class TasksListAPI(APIView):
    def get(self, request):
        get_tasks = Task.objects.all().order_by('due_date')
        tasks_list = []
        for task in get_tasks:
            sub_tasks_list = []
            for sub_task in task.sub_tasks:
                sub_task_data = {
                    "title"         : sub_task.title,
                    "description"   : sub_task.description,
                    "isCompleted"   : sub_task.is_completed
                }
                sub_tasks_list.append(sub_task_data)
            # print(task.id)
            task_data = {
                "ObjectId"      : str(task.id),
                "title"         : task.title,
                "description"   : task.description,
                "dueDate"       : task.due_date,
                "isCompleted"   : task.is_completed,
                "isDeleted"     : task.is_deleted,
                "subTasks"      : sub_tasks_list
            }
            tasks_list.append(task_data)
        return Response({"data":tasks_list, "message":"Task listed successfully","requestStatus":1},status=status.HTTP_200_OK)


class SearchTaskAPI(APIView):
    def post(self, request):    
        serializer = SearchTaskSerializer(data=request.data)
        if serializer.is_valid():
            query = Q(title__contains = request.data["searchQuery"])
            
            if request.data["isThisWeek"] == True:
                this_week = this_week_range(datetime.date.today())
                # print(this_week)
                query = query & Q(due_date__gte    =  this_week[0]) & Q(due_date__lte  =  this_week[1])
               
            else:
                pass
            
            if request.data["isNextWeek"] == True:
                next_week = next_week_range(datetime.date.today())
                # print(next_week)
                query = query & Q(due_date__gte    =  next_week[0]) & Q(due_date__lte  =  next_week[1])
            else:
                pass
            
            if request.data["isToday"] == True:
                query = query & Q(due_date = datetime.date.today())
            else:
                pass

            if request.data["isOverDue"] == True:
                query = query & Q(due_date__lt = datetime.date.today())
            else:
                pass

            get_tasks = Task.objects(query)
                        
            tasks_list = []
            for task in get_tasks:
                sub_tasks_list = []
                for sub_task in task.sub_tasks:
                    sub_task_data = {
                        "title"         : sub_task.title,
                        "description"   : sub_task.description,
                        "isCompleted"   : sub_task.is_completed
                    }
                    sub_tasks_list.append(sub_task_data)

                task_data = {
                    "ObjectId"      : str(task.id),
                    "title"         : task.title,
                    "description"   : task.description,
                    "dueDate"       : task.due_date,
                    "isCompleted"   : task.is_completed,
                    "isDeleted"     : task.is_deleted,
                    "subTasks"      : sub_tasks_list
                }
                tasks_list.append(task_data)
            return Response({"data":tasks_list, "message":"Task listed successfully","requestStatus":1},status=status.HTTP_200_OK)
        return Response({"data":serializer.errors,"message":"enter all required fields","requestStatus":0},status=status.HTTP_400_BAD_REQUEST)


class TaskDetailAPI(APIView):
    def post(self, request):
        serializer = TaskDetailSerializer(data=request.data)
        if serializer.is_valid():
            task = Task.objects.get(id=request.data["id"])
            sub_tasks_list = []
            for sub_task in task.sub_tasks:
                sub_task_data = {
                    "title"         : sub_task.title,
                    "description"   : sub_task.description,
                    "isCompleted"   : sub_task.is_completed
                }
                sub_tasks_list.append(sub_task_data)
            task_data = {
                    "ObjectId"      : str(task.id),
                    "title"         : task.title,
                    "description"   : task.description,
                    "dueDate"       : task.due_date,
                    "isCompleted"   : task.is_completed,
                    "isDeleted"     : task.is_deleted,
                    "subTasks"      : sub_tasks_list
                }
            return Response({"data":task_data, "message":"Task listed successfully","requestStatus":1},status=status.HTTP_200_OK)
        return Response({"data":serializer.errors,"message":"enter all required fields","requestStatus":0},status=status.HTTP_400_BAD_REQUEST)


class TaskUpdateAPI(APIView):
    def post(self,request):
        serializer = UpdateTaskSerializer(data=request.data)
        if serializer.is_valid():
            task_obj = Task.objects.get(id=request.data["id"])
            sub_tasks_list = []
            for item in range(0, len(request.data["subTasks"])):
                sub_task_item = SubTasks(
                    title           = request.data["subTasks"][item]["title"],
                    description     = request.data["subTasks"][item]["description"],
                    is_completed    = request.data["subTasks"][item]["isCompleted"],
                )
                sub_tasks_list.append(sub_task_item)
            task_obj.title           = request.data["title"]
            task_obj.description     = request.data["description"]
            task_obj.due_date        = request.data["dueDate"]
            task_obj.is_completed    = request.data["isCompleted"]
            task_obj.is_deleted      = request.data["isDeleted"]           
            task_obj.sub_tasks       = sub_tasks_list
            task_obj.deleted_on      = datetime.date.today()
            task_obj.save()
            return Response({"message":"Task Updated successfully","requestStatus":1},status=status.HTTP_200_OK)
        return Response({"data":serializer.errors,"message":"enter all required fields","requestStatus":0},status=status.HTTP_400_BAD_REQUEST)



from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger


import datetime
from datetime import timedelta


from . models import Task
logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/1')),
    name="delete_old_task",
    ignore_result = True
)
def delete_old_task():
    task_items = Task.objects.filter(
            is_deleted      = True,
            deleted_on__lte = datetime.date.today() - timedelta(25))
    task_items.delete()
    logger.info("Deleted old tasks")
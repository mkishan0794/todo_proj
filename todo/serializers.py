from rest_framework import serializers
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        


class SubTaskItem(serializers.Serializer):
    title               =   serializers.CharField(required=True)
    description         =   serializers.CharField(required=True)

class AddToDoSerializer(serializers.Serializer):
    title               =   serializers.CharField(required=True)                                                                                                                                                                                                                                                        
    description         =   serializers.CharField(required=True)
    dueDate             =   serializers.DateField(required=True)
    subTasks            =   SubTaskItem(many=True)
    
class SearchTaskSerializer(serializers.Serializer):
    searchQuery         =   serializers.CharField(allow_blank=True)
    isThisWeek          =   serializers.BooleanField()
    isNextWeek          =   serializers.BooleanField()
    isToday             =   serializers.BooleanField()
    isOverDue           =   serializers.BooleanField()

class TaskDetailSerializer(serializers.Serializer):
    id                  =   serializers.CharField()

class UpdateSubTaskItem(serializers.Serializer):
    title               =   serializers.CharField(required=True)
    description         =   serializers.CharField(required=True)
    isCompleted         =   serializers.BooleanField(required=False, default=False)

class UpdateTaskSerializer(serializers.Serializer):
    id                  =   serializers.CharField()
    title               =   serializers.CharField(required=True)
    description         =   serializers.CharField(required=True)
    dueDate             =   serializers.DateField(required=True)
    subTasks            =   UpdateSubTaskItem(many=True)
    isCompleted         =   serializers.BooleanField(required=False, default=False)
    isDeleted           =   serializers.BooleanField(required=False, default=False)
